;;; ob-perl.el --- Org-babel functions for Perl  -*- lexical-binding: t; -*-

;; Copyright (C) 2020 Francisco Jurado

;; Author: Francisco JUrado
;; Maintainer: Francisco Jurado
;; Created: August 2020
;; Keywords: perl, org, babel
;; Homepage: https://github.com/quicoju/ob-perl
;; Version: 0.0.1

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; Org-Babel support for evaluating Perl code.
;;
;; This piece of code is modeled after `ob-rust'.  Just like the `ob-rust', you
;; can specify :flags headers which will be passed to the perl interpreter.
;; Also you can also specify an :args which will be passed as a list of
;; arguments "@ARGV" to pass to the code snippet.
;;
;; `ob-perl' will always prepend the "strict" and "warnings" pragma to your code
;; snippet as a commodity.
;;
;; very limited implementation:
;; - currently only support :results output.

;;; Requirements:

;; - The perl interpreter
;; - Optional: GNU Make to facilitate running the tests

(require 'ob)
(require 'ob-eval)

;; optionally define a file extension for this language
(defvar org-babel-tangle-lang-exts)
(add-to-list 'org-babel-tangle-lang-exts '("perl" . "pl"))

(defvar org-babel-default-header-args:perl '())

(defun org-babel-execute:perl (body params)
  "Execute BODY as a block of Template code with org-babel.
This function is called by `org-babel-execute-src-block' where
BODY and PARAMS are passed as arguments to it."
  (message "executing Perl source code block")
  (let* ((tmp-src-file (org-babel-temp-file "perl-src-" ".pl"))
         (processed-params (org-babel-process-params params))
         (flags (or (cdr (assoc :flags processed-params)) ""))
         (args  (or (cdr (assoc :args  processed-params)) ""))
         (coding-system-for-read  'utf-8)
         (coding-system-for-write 'utf-8)
         (body-with-pragmas (concat "use strict;\nuse warnings;\n" body "\n")))
    (with-temp-file tmp-src-file (insert body-with-pragmas))
    (let* ((command (format "perl %s %s %s" flags tmp-src-file args))
           (results (org-babel-eval command "")))
      (when results
        (org-babel-reassemble-table
         (if (or (member "table"  (cdr (assoc :result-params processed-params)))
                 (member "vector" (cdr (assoc :result-params processed-params))))
             (let ((tmp-file (org-babel-temp-file "perl-")))
               (with-temp-file tmp-file (insert (org-trim results)))
               (org-babel-import-elisp-from-file tmp-file))
           (org-babel-read (org-trim results) t))
         (org-babel-pick-name
          (cdr (assoc :colname-names params)) (cdr (assoc :colnames params)))
         (org-babel-pick-name
          (cdr (assoc :rowname-names params)) (cdr (assoc :rownames params))))))))

;; This function should be used to assign any variables in params in
;; the context of the session environment.
(defun org-babel-prep-session:perl (_session _params)
  "This function does nothing as Perl does't have support for sessions."
  (error "Perl doesn't have support for sessions"))

(provide 'ob-perl)
;;; ob-perl.el ends here

