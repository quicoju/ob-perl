.PHONY: test clean

clean:
	@rm -f .test-org-id-locations

test: clean
	@emacs -Q --batch \
	      -L . \
	      -l ert \
	      -l test-ob-perl.el \
	      -f ert-run-tests-batch-and-exit
