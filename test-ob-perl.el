;;; test-ob-perl.el --- tests for ob-perl.el

;; Org-Babel support for evaluating Perl code.
;; 
;; ThiS code is modeled after `ob-rust'. And is a work in progress

(require 'ert)
(require 'org)
(require 'org-id)
(require 'ob-perl)

(defconst ob-perl-test-dir
  (expand-file-name (file-name-directory (or load-file-name buffer-file-name))))

(defconst org-id-locations-file
  (expand-file-name ".test-org-id-locations" ob-perl-test-dir))

(defun ob-perl-test-update-id-locations ()
  (let ((files (directory-files
                ob-perl-test-dir 'full
                "^\\([^.]\\|\\.\\([^.]\\|\\..\\)\\).*\\.org$")))
    (org-id-update-id-locations files)))

(defmacro org-test-at-id (id &rest body)
  "Run BODY after placing the point in the headline identified by ID."
  (declare (indent 1))
  `(let* ((id-location (org-id-find ,id))
	  (id-file (car id-location))
	  (visited-p (get-file-buffer id-file))
	  to-be-removed)
     (unwind-protect
	 (save-window-excursion
	   (save-match-data
	     (org-id-goto ,id)
	     (setq to-be-removed (current-buffer))
	     (condition-case nil
		 (progn
		   (org-show-subtree)
		   (org-show-block-all))
	       (error nil))
	     (save-restriction ,@body)))
       (unless (or visited-p (not to-be-removed))
	 (kill-buffer to-be-removed)))))

(def-edebug-spec org-test-at-id (form body))

(ert-deftest ob-perl/basic ()
  "Test basic usage"
  (let (org-confirm-babel-evaluate)
    (ob-perl-test-update-id-locations)
    (org-test-at-id "5947c402da07c7aca0000001"
      (org-babel-next-src-block)
      (should (string-equal "hello, ob-perl" (org-babel-execute-src-block))))))

(ert-deftest ob-perl/flags ()
  "Test the usage of flags and args."
  (let (org-confirm-babel-evaluate)
    (ob-perl-test-update-id-locations)
    (org-test-at-id "5947c402da07c7aca0000002"
      (org-babel-next-src-block)
      (should (string-equal "hello, ob-perl" (org-babel-execute-src-block))))))

(provide 'test-ob-perl)
;;; test-ob-perl.el ends here
