
# ob-perl.el

ob-perl.el adds [org-babel](http://orgmode.org/worg/org-contrib/babel/intro.html) support to evaluate [Perl](https://www.perl.org/) code.

This package is based on the [ob-rust](https://melpa.org/#/ob-rust) implementaiton.


<a id="org381b97b"></a>

## Usage

    #+BEGIN_SRC perl :flags -MData::Dumper :args one two
     print Dumper(\@ARGV)

    #+RESULTS:
    : $VAR1 = [
    :           'one',
    :           'two'
    :         ];
    #+END_SRC


<a id="org491ec23"></a>

## Requirements

1.  The [perl](https://www.perl.org/) interpreter
2.  Optional: [GNU Make](https://www.gnu.org/software/make/) (for facilitate running tests)


<a id="org9f4b8c1"></a>

## Installation

Place ob-perl.el in your load path

